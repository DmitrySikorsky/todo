/**
 * Created by user on 4/10/17.
 */
$(document).ready(function() {
    var data = {};



    function dataSort() {

    }

    //функция для первода числа месяца в двузначный вид
    function formatingDate(date){
        return date < 10 ? "0"+date : date;
    }

    // функция ждя перевода даты в вид: DD-MM-YYYY HH:mm
    function getNewDate() {
        var normalDate = new Date();
        return (formatingDate(normalDate.getDate())) + '-' +
               (formatingDate(normalDate.getMonth() + 1 )) + '-'
                + normalDate.getFullYear() + ' ' +
               (formatingDate(normalDate.getHours())) + ':' +
               (formatingDate(normalDate.getMinutes()));
    }


    //Сохранение данных в локальном хранилище
    function saveData() {
        localStorage.setItem('formData', JSON.stringify(data));
    }

    //выгрузка данных из локального хранилища и перевод в норм вид
    function loadData() {
        data = localStorage.getItem('formData');
        if (data) {
            data = JSON.parse(data);
        } else {
            data = {}
        }
    }

    //перерисовка таблицы в случае перезагрузки страницы
    function reloadTable() {
        $('.table').html('');

        for (var i in data) {
            if (!data[i]) {continue;}

            //консоль для просмотра событий и отладки
            console.log(data[i]);

            //объявление строки таблицы
            var newRow = $('<tr>');


            //чекбокс
            var checkbox = $('<td><input type="checkbox">')
                .data('id', i);



            //объявление заголовка
            var title = $('<td>')
                .addClass('editable')
                .data('field', 'title')
                .data('id', i)
                .text(data[i].title);

            //автор
            var author = $('<td>')
                .addClass('editable')
                .data('field', 'author')
                .data('id', i)
                .text(data[i].author);

            //дата изменения
            var uptated = $('<td>')
                .data('field', 'updated')
                .data('id', i)
                .text(data[i].updated);

            //кнопка удаления
            var deleteItem = $('<td>')
                .addClass('delete')
                .data('field', 'updated')
                .data('id', i)
                .text('Delete');

            //содание строки
            newRow.append(checkbox);
            newRow.append(title);
            newRow.append(author);
            newRow.append(uptated);
            newRow.append(deleteItem);

            //добавление строки к таблице
            $('.table').append(newRow);
        }
    }

    //объявление функции редактирование элементов ячеек по двойному клику
    $('.table').delegate(".delete", 'click', function () {
        var cellData = $(this).data();
        delete data[cellData.id];

        saveData();
        reloadTable();
    });

    //функция редактирования
    $('.table').delegate("td", 'dblclick', function () {
        if (!$(this).hasClass('editable')) {
            return false;
        }


        var OriginalContent = $(this).text();


        $(this).addClass("cellEditing");
        $(this).html("<input type='text' size='5' value='" + OriginalContent + "' />");
        $(this).children().first().focus();

        //сохранение текста в ячейке после нажатия энтера(номер 13 кнопка)
        $(this).children().first().keypress(function (e) {
            if (e.which == 13) {
                var newContent = $(this).val();

                if (newContent.length > 0 && newContent.trim().length > 0) {  //проверка на наличие строки в редактируемой ячейке
                    var cellData = $(this).parent().data();

                    console.log(cellData, data);

                    data[cellData.id][cellData.field] = $(this).val();
                    data[cellData.id]['updated'] = getNewDate();           //изменение даты последнего редактирования при изменении в таблице

                    saveData();     //сохрвнение в локал
                    loadData();     // загрузка из локал
                    reloadTable();  //перерисовка таблицы

                    $(this).parent().text(newContent);
                }

                $(this).parent().removeClass("cellEditing");
            }
        });
    });


    //введение данных из формы
    $('.form').on('submit', function(event) {
        event.preventDefault();         //предотвращение перенаправление действия куда-то..
        event.stopPropagation();        //остановка, предотвращение перезагрузки таблицы

        if ($('.title').val().trim().length > 0 &&      //проверка на наличие текста в форме, учитывая пробелы вместо оного
            $('.author').val().trim().length > 0) {

            //элемент массива для хранения в локальном хранилище
            var elem = {
                id: new Date(),
                title: $('.title').val(),
                author: $('.author').val(),
                status: false,
                updated: getNewDate()
            }

            //запись в джейсон формат
            data[JSON.stringify(elem.id)] = elem;

            //запись в локальное
            saveData();

            //загрузка из локального
            loadData();

            //перерисовка таблицы
            reloadTable();

            //в случае отсутствия текста в одной из строк алерт ошибки
        } else {
            alert("Please, don't forget enter data into form!");
        }

        return false;
    });

    loadData();
    reloadTable();
});